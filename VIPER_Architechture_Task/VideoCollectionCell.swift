//
//  VideosListViewCell.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit
import AlamofireImage

class VideoCollectionCell: UICollectionViewCell{
    // MARK: *** UI Elements
    @IBOutlet weak var epsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    
    func set(forVideos video: VideoModel){
        nameLabel?.text = video.name
        epsLabel?.text = video.eps
        let url = URL(string: video.imgUrl)!
        videoImageView.af_setImage(withURL: url)
    }
}
