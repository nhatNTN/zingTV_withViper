//
//  VideoDetailPresenter.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 12/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation

class VideoDetailPresenter: VideoDetailPresenterProtocol {
    
    weak var view: VideoDetailViewProtocol?
    var wireFrame: VideoDetailWireFrameProtocol?
    var video: VideoModel?
    
    func viewDidLoad() {
        view?.showVideoDetail(forVideo: video!)
    }
    
}
