//
//  VideosListPresenter.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation

class VideosListPresenter: VideosListPresenterProtocol{
    weak var view: VideosListProtocol?
    var interactor: VideosListInteractorInputProtocol?
    var wireFrame: VideosListWireFrameProtocol?
    
    func viewDidLoad() {
        view?.showLoading()
        interactor?.retrieveVideosList()
    }
    
    func showVideoDetail(forVideo video: VideoModel) {
        wireFrame?.presentVideosDetailScreen(from: view!, forVideo: video)
    }
}

extension VideosListPresenter: VideosListInteractorOutputProtocol {
    func didRetrieveVideos(_ videos: [VideoModel]) {
        view?.hideLoading()
        view?.showVideos(with: videos)
    }
    
    func onError() {
        view?.hideLoading()
        view?.showError()
    }
}
