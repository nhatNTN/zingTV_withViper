//
//  VideoDetailView.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 12/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoDetailView: UIViewController{
    // MARK: *** UI Elements
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var nameVideo: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var loveButton: UIButton!
    
    var presenter: VideoDetailPresenterProtocol?
    var buttonLoveIsSelected = false
    
    // MARK: *** UI Events
    @IBAction func tapLoveButton(_ sender: Any) {
        buttonLoveIsSelected = !buttonLoveIsSelected
        updateLoveButton()
    }
    
    func updateLoveButton() {
        if buttonLoveIsSelected {
        loveButton.setImage(UIImage(named: "likeHitted"), for: UIControlState.normal)
        }
        else {
        loveButton.setImage(UIImage(named: "likeHit"), for: UIControlState.normal)
        }
    }

    // MARK: *** UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        presenter?.viewDidLoad()
    }
}
extension VideoDetailView: VideoDetailViewProtocol{
    func showVideoDetail(forVideo video: VideoModel) {
        let url = URL(string: video.imgUrl)!
        videoImageView.af_setImage(withURL: url)
        self.nameVideo.text = video.name
        self.descriptionTextView.text = video.des
        self.typeLabel.text = video.type
        self.scheduleLabel.text = video.schedule
    }
}
